<div class="activity_feeds">
	<div id="tabs">
	<ul>
		<?php if(isset($activity_feeds_items['facebook'])): ?>		
		<li> <a href="#facebook"><img src="<?php print base_path(); print $activity_feeds_module_path ?>/images/facebook1.png" /></a> </li>
		<?php endif; ?>
	
		<?php if(isset($activity_feeds_items['twitter'])): ?>	
		<li> <a href="#twitter"><img src="<?php print base_path(); print $activity_feeds_module_path ?>/images/twitter.png" /></a> </li>
		<?php endif; ?>

		<?php if(isset($activity_feeds_items['linkedin'])): ?>	
		<li> <a href="#linkedin"><img src="<?php print base_path(); print $activity_feeds_module_path ?>/images/linkedin.png" /></a> </li>
		<?php endif; ?>	
	</ul>
	
		
	<?php if(isset($activity_feeds_items['facebook'])): ?>	
	<div id="facebook">
	  <?php foreach($activity_feeds_items['facebook'] as $facebook): ?>
	       <?php if(!empty($facebook)): ?>
 
		<p class="afp">
			<span class="activityfeed_text">
		  		<a href="http://facebook.com/<?php print $facebook['id'] ?>"> 
		   			<?php print $facebook['author']; ?>: 
		  		</a>			
		  		<?php print $facebook['text']; ?>
			</span>
			<br />
			<span class="activityfeed_date"> <?php print $facebook['date']; ?> </span>
			<hr />
	      </p>
		<?php endif; ?>		
	    <?php endforeach; ?>
	</div>
	<?php endif; ?>	

	<?php if(isset($activity_feeds_items['twitter'])): ?>	
	<div id="twitter">
		<?php foreach($activity_feeds_items['twitter'] as $twitter): ?>
			<?php if(!empty($twitter)): ?>	
			<p class="afp">
				<span class="activityfeed_text"> 
					<a href="https://twitter.com/#!/<?php print $twitter['author'] ?>"> @<?php print $twitter['author'] ?>: </a>
					<?php print $twitter['text']; ?> 
				</span>
				<br />
				<span class="activityfeed_date"> <?php print $twitter['date']; ?> </span>
				<hr />	
			</p>
			<?php endif; ?>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>	
	
	<?php if(isset($activity_feeds_items['linkedin'])): ?>	
	<div id="linkedin">
		<?php foreach($activity_feeds_items['linkedin'] as $linkedin): ?>
			<p class="afp"> 	
				<span class="activityfeed_text"> <?php print $linkedin['text']; ?> </span>
				<br />
				<span class="activityfeed_date"> <?php print $linkedin['date']; ?> </span>
				<hr />
			</p>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
	</div>
</div>
