<?php

class Activity_Feeds {

   private $ch;
   private $facebook_url = "https://graph.facebook.com";
   private $twitter_url = "http://api.twitter.com/1/statuses/user_timeline.json?screen_name=";
   private $entries;

   private $dates = array('Jan' => '01',
			  'Feb' => '02',
			  'Mar' => '03',
			  'Apr' => '04',
			  'May' => '05',
			  'Jun' => '06',
			  'Jul' => '07',
			  'Aug' => '08',
			  'Sep' => '09',
			  'Oct' => '10',
			  'Nov' => '11',
			  'Dec' => '12');
	
   /**
    * Initiates the curl library
    * sets the number of entries to 
    * display
    */
   public function __construct($entries){
     $this->entries = $entries;
     $this->ch = curl_init();
     curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true); 
   }

  public function get_facebook_feed($id, $token){
    $events = array();
    curl_setopt($this->ch, CURLOPT_URL, "{$this->facebook_url}/$id/feed?access_token=$token");
    $feed = curl_exec($this->ch);
    $filtered_feed = json_decode($feed);
    $current_entry = 0;
    foreach($filtered_feed->data as $event){
      // determine if we retrieved enough entries
      // and  exit and exit if we did
      if($current_entry > ($this->entries -1)) { break; }
      
      if(isset($event->message)) {
		$text = $event->message;
		curl_setopt($this->ch, CURLOPT_URL, "{$this->facebook_url}/{$event->from->id}");
		$user_feed = curl_exec($this->ch);
		$filtered_user_feed = json_decode($user_feed);
		
		//var_dump($filtered_user_feed);
		
		$username = "";
		if(isset($filtered_user_feed->username)){
			$username = $filtered_user_feed->username;
		} else {
			$username = $filtered_user_feed->name;
		}
		$user_id = $filtered_user_feed->id;
		$ids = explode("_", $event->id);
		$eventid = $ids[1];
		// fix the date
		$pattern = '/^(\d\d\d\d)-(\d\d)-(\d\d)T(\d\d):(\d\d):(\d\d)/';
		preg_match($pattern, $event->created_time, $matches);
		$date = "{$matches[2]}-{$matches[3]}-{$matches[1]} {$matches[4]}:{$matches[5]}:{$matches[6]}";
		$events[] = array( 'photo' => "{$this->facebook_url}/$user_id/picture",
	              	   	   'author'=> $username,
		      	  	   'text'  => $text,
		      	   	   'date'  => $date,
				   'id'    => $user_id,
		    		);
		$current_entry += 1;
      }
    }
    return $events;
  }

  public function get_twitter_feed($id){
   $events = array();
   curl_setopt($this->ch, CURLOPT_URL, $this->twitter_url.$id);
   $feed = curl_exec($this->ch);
   $filtered_feed = json_decode($feed);
   $current_entry = 0;
   foreach($filtered_feed as $tweet){
     if($current_entry > $this->entries -1) {break; }	
        // split the text by spaces
        $keywords = preg_split("/\s+/", $tweet->text);                      
        // search for '@' in the array
	// also need to find # in the array
        for($i=0;$i<sizeof($keywords);$i++){
                $pattern = '/(^@([\w]+))/';
                if(preg_match($pattern, $keywords[$i], $match)){
                        $keywords[$i] = "<a href=\"https://twitter.com/#!/{$match[2]}\"> {$match[1]} </a>";
                }
        }
	$pattern = '/^(\w+)\s(\w+)\s(\d\d)\s(\d\d):(\d\d):(\d\d)\s\+0000\s(\d\d\d\d)$/';
       	preg_match($pattern,$tweet->created_at,$matches);
	$month = $this->dates["{$matches[2]}"];
	$date = "{$month}-{$matches[3]}-{$matches[7]} {$matches[4]}:{$matches[5]}:{$matches[6]}"; 
	$text_string = implode(" ", $keywords);
  	$events[] = array( 'photo' => $tweet->user->profile_background_image_url,
			   'author'=> $tweet->user->screen_name,
			   'text'  => $text_string,
			   'date'  => $date,
		);	
     $current_entry += 1;
   } 
   return $events;	
  }

  public function get_linkedin_feed($rssurl){
    curl_setopt($this->ch, CURLOPT_URL, $rssurl);
    $feed = curl_exec($this->ch);
    $profileXML = simplexml_load_string($feed);
    $current_entry = 0;
    foreach($profileXML->channel->item as $item){ 
      if($current_entry > ($this->entries -1)) { break; }
      $events[] = array( 'title' => (string) $item->title,
			 'link'  => (string) $item->link,
			 'text'  => (string) $item->description,
			 'date'  => (string) $item->pubDate,
		  );
      $current_entry += 1;
    } 
    return $events;
  }
}

