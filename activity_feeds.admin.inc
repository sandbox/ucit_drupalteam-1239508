<?php

/**
 * @file
 * Administration page callbacks for the activity_feeds module
 */

/**
 * Form builder, Configure activity_feeds
 *
 * @ingroup 
 * @see system_settings_form()
 */
function activity_feeds_admin_settings() {
 global $base_url; 
 $form['activity_feeds'] = array(); 
 
  #### facebook settings form #### 
  $fb_settings  = db_query("SELECT * FROM {activity_feeds_facebook_options}")->fetchObject();
  $form['activity_feeds']['facebook_settings'] = array(
    '#type'        => 'fieldset',
    '#title'       => 'Facebook Settings',
    '#weight'      => 5,
    '#collapsed'   => FALSE,
    '#collapsible' => TRUE,
   );
   $form['activity_feeds']['facebook_settings']['facebook_settings_enabled'] = array(
      	'#type' 	       => 'checkbox',
      	'#title' 	       => 'Enable facebook feed',
      	'#default_value'       => ($fb_settings->enabled) ? true: false,
   );

   $form['activity_feeds']['facebook_settings']['facebook_settings_appid'] = array(
	'#type'		      => 'textfield',
	'#title'	      => 'Application ID',
    	'#default_value'      => ($fb_settings->app_id) ? $fb_settings->app_id: "",
	'#description'	      => "In order for the facebook feed to work, you must first create an application on the <a href='https://developers.facebook.com/apps'> Facebook Developers page </a>.
				 The only thing you will need to specify is the url that you will be using the application from ( i.e web.drupal.com )  in the web settings of the application.",
   );
  
   $form['activity_feeds']['facebook_settings']['facebook_settings_appsecret'] = array(
	'#type'		      => 'password',
	'#title'	      => 'Application Secret',
    	'#default_value'      => ($fb_settings->app_secret) ? $fb_settings->app_secret : "",
	'#description'	      => "In order for the facebook feed to work, you must first create an application on the <a href='https://developers.facebook.com/apps'> Facebook Developers page </a>.
				 The only thing you will need to specify is the url and domain that you will be using the application from ( i.e web.drupal.com )  in the web settings of the application.",
   );

   $form['activity_feeds']['facebook_settings']['facebook_settings_token'] = array(
	'#type'			=> 'textfield',
	'#title'		=> 'Authentication Token',
	'#disabled'		=> TRUE,
	'#id'			=> 'clear_facebook_text',
	'#default_value'	=> ($fb_settings->token) ? $fb_settings->token : "",	
	'#description' 		=> "This field will be auto populated after submitting the form. If you want to reauthorize facebook <a id='clear_facebook_link'> clear the field </a>", 
   );
   $form['activity_feeds']['facebook_settings']['facebook_settings_user_group_id'] = array(
	'#type'		 => 'textfield',
	'#title' 	 => 'User/Group ID',
	'#default_value' => ($fb_settings->user_id)? $fb_settings->user_id : "",
	'#required'	 => false,
   );
      

  #### twitter settings form ####
  $twit_settings = db_query("SELECT * FROM {activity_feeds_twitter_options}")->fetchObject();

  $form['activity_feeds']['twitter_settings'] = array(
    '#type'    	  => 'fieldset',
    '#title'   	  => 'Twitter Settings',
    '#weight'  	  => 6,
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );
  $form['activity_feeds']['twitter_settings']['twitter_settings_enabled'] = array(
    '#type' 	       => 'checkbox',
    '#title' 	       => 'Enable twitter feed',
    '#default_value'   => $twit_settings->enabled ? true : false,

  );		
  $form['activity_feeds']['twitter_settings']['twitter_settings_user_id'] = array(
    '#type'          => 'textfield',
    '#title' 	     => 'User ID',
    '#default_value' => $twit_settings->username ? $twit_settings->username : "",
    '#required'	     => false, 
  );

  #### linkedin settings form ####
  $lkin_settings = db_query("SELECT * FROM {activity_feeds_linkedin_settings}")->fetchObject();
  $form['activity_feeds']['linkedin_settings'] = array(
    '#type'    	  => 'fieldset',
    '#title'   	  => 'LinkedIn Settings',
    '#weight'  	  => 7,
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );
  $form['activity_feeds']['linkedin_settings']['linkedin_settings_enabled'] = array(
    '#type' 	       => 'checkbox',
    '#title' 	       => 'Enable linkedin feed',
    '#default_value'   => $lkin_settings->enabled ? true : false,
  ); 
  $form['activity_feeds']['linkedin_settings']['linkedin_settings_rss_feed'] = array(
    '#type'          => 'textfield',
    '#title' 	     => 'RSS Feed URL',
    '#default_value' => $lkin_settings->feed_url ? $lkin_settings->feed_url : "",
    '#required'	     => false,
    '#description'   => "To get your rss feed url visit <a href='http://www.linkedin.com/rssAdmin?display'>LinkedIn rssAdmin </a> 
			 and under Network Updates choose 'enable'. You will see a text box now populated with a url. Copy the url
			 paste it here." 

  );
  $form['#submit'][] = 'activity_feeds_admin_settings_submit'; 
  return system_settings_form($form);

}


/**
 * Form Validation
 */
function activity_feeds_admin_settings_validate($form, &$form_state){
  if($form_state['values']['facebook_settings_enabled']){
    // Make sure the user also set app_id, app_secret and user/group id
    if(!$form_state['values']['facebook_settings_appid']){
      form_set_error('facebook_settings_appid', t("Application ID must be set to enable Facebook"));
    } 
    if(!$form_state['values']['facebook_settings_appsecret']){
      form_set_error('facebook_settings_appsecret', t("Application Secret must be set to enable Facebook"));
    }
    if(!$form_state['values']['facebook_settings_user_group_id']){
      form_set_error('facebook_settings_user_group_id', t("User/Group ID must be set to enable Facebook"));
    }
  }
  
  // validate twitter and linked in
  if($form_state['values']['linkedin_settings_enabled']){
    if(!$form_state['values']['linkedin_settings_rss_feed']){
      form_set_error('linkedin_settings_rss_feed', t('The RSS Feed URL must be set to enable LinkedIn'));
    }
  }

  if($form_state['values']['twitter_settings_enabled']){
    if(!$form_state['values']['twitter_settings_user_id']){
      form_set_error('twitteer_settings_user_id', t("The Username must be set to enable Twitter"));
    }
  }

}

/**
 * Process the activity feeds admin settings submission
 */
function activity_feeds_admin_settings_submit($form, &$form_state){
  global $base_url; 
  $time = time();
  db_delete('activity_feeds_facebook_options')->isNotNull('facebook_id')->execute(); // Should delete everything
  db_delete('activity_feeds_twitter_options')->isNotNull('twi_id')->execute();  
  db_delete('activity_feeds_linkedin_settings')->isNotNull('linkedin_id')->execute();

  // Enter the facebook settings in the database
  $fb_id = db_insert('activity_feeds_facebook_options')
		->fields(array(
		  'enabled' => $form_state['values']['facebook_settings_enabled'],
		  'app_id'  => $form_state['values']['facebook_settings_appid'],
		  'token'   => $form_state['values']['facebook_settings_token'] ? $form_state['values']['facebook_settings_token'] : "", 
 	          'app_secret' => $form_state['values']['facebook_settings_appsecret'],
	          'user_id'   => $form_state['values']['facebook_settings_user_group_id'],		   
		))->execute();

  // Enter the twitter settings in the database
  $twit_id = db_insert('activity_feeds_twitter_options')
		->fields(array(
		  'enabled'      => $form_state['values']['twitter_settings_enabled'],
		  'username'     => $form_state['values']['twitter_settings_user_id'],
		  'last_updated' => $time, 
		))->execute();

  // Enter the linked in settings in the database
  $lkin_id = db_insert('activity_feeds_linkedin_settings')
		->fields(array(
		  'enabled'  => $form_state['values']['linkedin_settings_enabled'],
		  'feed_url' =>	$form_state['values']['linkedin_settings_rss_feed'],
		))->execute();


  //If facebook enabled is set and token is not then setup the token
  if($form_state['values']['facebook_settings_enabled'] && empty($form_state['values']['facebook_settings_token'])){
	$form_state['redirect'] = "https://www.facebook.com/dialog/oauth?client_id={$form_state['values']['facebook_settings_appid']}&redirect_uri={$base_url}/activity_feeds/facebook/register&scope=read_stream,offline_access"; 
  }

}

/**
 * Register for facebook access
 */
function activity_feeds_register_facebook($code=""){
  watchdog('activity_feeds', 'In the other file'); 
  global $base_url;
  if(isset($_GET['code'])){ $code = $_GET['code']; } 
  // now we need to authorize the application
  $appid = variable_get('facebook_settings_appid',"");
  $appsecret = variable_get('facebook_settings_appsecret',""); 
  $url = $base_url."/activity_feeds/facebook/register"; 
  if($appid == "" || $appsecret == ""){
  	drupal_goto($base_url."/admin/config/content/activity_feeds"); 
  } else {  
        $ch = curl_init();
     	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
	curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/oauth/access_token?client_id={$appid}&redirect_uri={$url}&client_secret={$appsecret}&code={$code}");
    	$token_whole = curl_exec($ch);
	$token = explode("=", $token_whole);
	db_update('activity_feeds_facebook_options')->fields(array('token' => $token[1]))->isNotNull('facebook_id')->execute();
        drupal_goto($base_url."/admin/config/content/activity_feeds"); 
  }
}
